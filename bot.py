#!/usr/bin/python
import telebot
import datetime
from mk_livestatus import Socket

# dados de conexao com a API do Telegram e o Livestatus
bot = telebot.TeleBot("API_KEY")
s = Socket(('127.0.0.1', 50000))

# handler para as mensagens start e help
@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.reply_to(message, "\nComandos disponiveis:\n/offline: Mostra os hosts offline\n/online: Mostra os hosts online")

# handler para a mensagem down
@bot.message_handler(commands=['down', 'offline'])
def send_offline(message):
    # obtem os names de hosts com state 1 (down) e sem acknowledge
    q = s.hosts.columns('name').filter('state = 1').filter('acknowledged = 0')
    r = q.call()
    l = ""
    for host in r:
        l += "%s\n" % (host[0])
    bot.reply_to(message, "\nHosts offline em " + datetime.datetime.now().strftime('%d/%m/%Y %H:%M') + "\n\n" + l)

# handler para a mensagem up
@bot.message_handler(commands=['up', 'online'])
def send_online(message):
    # obtem os names de hosts com state 0 (up)
    q = s.hosts.columns('name').filter('state = 0')
    r = q.call()
    l = ""
    for host in r:
        l += "%s\n" % (host[0])
    bot.reply_to(message, "\nHosts online em " + datetime.datetime.now().strftime('%d/%m/%Y %H:%M') + "\n\n" + l)

# habilita o polling
bot.polling()

