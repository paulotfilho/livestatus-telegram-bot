## livestatus-telegram-bot

Este repositório é um wrapper do módulo MK Livestatus (https://mathias-kettner.de/checkmk_livestatus.html) suportado por sistemas de monitoramento baseados em Nagios para bots Telegram utilizando pyTelegramBotAPI (https://github.com/eternnoir/pyTelegramBotAPI), uma implementação Python simples mas muito extensível da API Telegram Bot (https://core.telegram.org/bots/api).

O código disponível neste repositório possui a licença Apache License 2.0.

#### **Dependências**

 - python-mk-livestatus v0.5-pop: Disponível em outro repositório meu. Clonar e instalar com "*python setup.py install*".
 - pyTelegramBotAPI: Disponível no pip. Instalar com "*pip install pyTelegramBotAPI*".
 - Token da API Telegram Bot: Obter com o BotFather (https://core.telegram.org/bots#botfather)
 - Módulo Livestatus para o sistema de monitoramento utilizado (Shinken: "*shinken install livestatus*")

#### **Instalação**

Para instalar o bot, clone o repositório para algum local que possa se comunicar com o Livestatus do sistema de monitoramento. Em seguida, abra o arquivo "bot.py" e insira o token da API do Telegram:

```sh
# troque API_KEY pelo token obtido com o BotFather
bot = telebot.TeleBot("API_KEY")
```

Para executar o bot em outro local que não a própria máquina do sistema de monitoramento, modifique o endereço IP do socket:

```sh
# troque 127.0.0.1 pelo IP do sistema de monitoramento
s = Socket(('127.0.0.1', 50000))
```

Em seguida, basta rodar o arquivo bot.py e contatar o bot registrado.

#### **Utilização**

O wrapper possui atualmente dois comandos implementados: 

 - /down OU /offline: Obtém uma lista com os nomes dos hosts em estado DOWN, sem acknowledge.
 - /up OU /online: Obtém uma lista com os nomes dos hosts em estado UP.